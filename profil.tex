\chapter{Profile}\label{cpt:Profile}

\section{Preamble}

\subsection{Naming Conventions}

The keywords "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119.
 
When describing abstract data models, this specification uses the notational convention used by the XML Infoset. Specifically, abstract property names always appear in square brackets (e.g., [some property]).
 
When describing concrete XML schemas, this specification uses a convention where each member of an element’s [children] or [attributes] property is described using an XPath-like notation (e.g., /x:MyHeader/x:SomeProperty/@value1).  The use of \{any\} indicates the presence of an element wildcard (\textless xs:any/\textgreater). The use of @{any} indicates the presence of an attribute wildcard (\textless xs:anyAttribute/\textgreater).
 
Readers are presumed to be familiar with the terms in the Internet Security Glossary [GLOS].

\subsection{Namespaces and prefixes}

The following namespaces with according prefixes are used in this document.

\begin{tabular}{|l|l|}
\hline ws4d & http://www.ws4d.org \\ 
\hline   &  \\ 
\hline   &  \\ 
\hline   &  \\ 
\hline 
\end{tabular} 

\section{Basic enhancements}


\subsection{Clients}
Although clients do not need to be addressable, they must be identifiable. For this purpose, a client \normativ{must} have a unique identifier that meets the same requirements as the unique identifiers for DPWS devices (e.g. urn:uuid:\textit{\{enter-uuid-here\}}). The client's identification happens implicitly through its Security Context Token (SCT).

\subsection{Discovery}
Due to the nature of both, Device Discovery and MetadataExchange need to be conducted before an authentication takes place. However, this must be considered as untrusted information and must thus me repeated after authentication took place.

\myrule{0001}{Information a client gathered about a device before authenticating with it \normativ{must} be considered untrusted. Gathering this information \normativ{must} be repeated in a trusted way before relying on it}

On several occasions a client is interested in trusted devices only when probing in a network.

\myrule{0002}{If a Client wants to search for trusted devices only, it \normativ{may} sign its PROBE message.}

\myrule{0003}{If a Client wants untrusted Devices to respond to a PROBE message, it \normativ{must not} sign the PROBE message}

\myrule{0004}{When a HOSTING SERVICE receives a signed PROBE message, it SHOULD only respond if it can verify the signature.}

\myrule{0005}{A Client \normativ{must not} rely on devices to only answer if they can verify the signature.}

Rule 0005 guarantees backwards compatibility.

\section{Sicherheit auf Nachrichtenebene}

\subsection{WS Compact Security}\label{cpt:appendix:profile:csec}

WS Compact Security bietet ein kompaktes Format zur Einbettung von Signaturen (s. Abschnitt~\ref{cpt:appendix:profile:csec:signatures}) und verschlüsselten Nachrichtenpassagen (s. Abschnitt~\ref{cpt:appendix:profile:csec:encryption}) in SOAP-Nachrichten. Beispielnachrichten finden sich in Abschnitt~\ref{cpt:appendix:profile:csec:samples} und in Abschnitt~\ref{cpt:appendix:profile:csec:algorithm} ist dargelegt, in welchen Schritten eine Nachricht signiert und oder verschlüsselt wird.

Die folgenden Überlegungen beziehen sich auf alle Nachrichten außer den Discovery-Verkehr. 

Nachrichten \normativ{sollten} immer signiert werden. Ein Empfänger \normativ{darf} beschließen, eine nicht signierte Nachricht zu verwerfen. Falls Teile einer Nachricht verschlüsselt werden, \normativ{sollte} die Nachricht signiert werden.

\subsubsection{Signaturen}\label{cpt:appendix:profile:csec:signatures}

Zur Einbettung einer Signatur kommt das WS-DD Compact Signature Format zum Einsatz, das in Web Services Dynamic Discovery (WS-DD), Version 1.1, in Kapitel 8.2 (\cite[Kap. 8.2]{WS-DD}) definiert wird. Als einzige Änderung an diesem Format wird die folgende Konvention eingeführt. Ist das \lstinline|refs|-Attribut leer oder fehlt es ganz, so enthält es implizit das gesamte \lstinline|<Envelope>|-Element, also die gesamte SOAP-Nachricht.

\subsubsection{Verschlüsselung}\label{cpt:appendix:profile:csec:encryption}

Um Passagen einer SOAP-Nachricht zu verschlüsseln und die Nachricht einzubetten, steht eine Kombination aus dem \lstinline|<enc>|-Headerfeld und einem \lstinline|<EncryptedData>|-Element im Body der Nachricht zur Verfügung.

Das  \lstinline|<enc>|-Headerfeld hat das folgende Format:
\begin{lstlisting}[language=XML,caption={ENC Header Feld}]
<ws4d:enc
    xmlns:ws4d="http://www.ws4d.org/ws4d-sec/2011/08"
    KeyId=...
    Refs=...
    Scheme=...
/>
\end{lstlisting}
\lstinline|KeyId| enthält eine Identifikation des verwendeten Schlüssels.\\
\lstinline|Refs| enthält die Liste der verschlüsselten Elemente. Ist dieses Attribut leer oder fehlt es ganz, so erhält es die ID des \lstinline|<Body>|-Elements.\\
\lstinline|Scheme| enthält das zur Verschlüsselung eingesetzte Schema. Bisher sind folgende Schemata definiert:

\begingroup
\noindent
\leftskip=0.5cm % ggf. verstellen
\lstinline|http://www.ws4d.org/ws4d-sec/2011/08/rc4|: Verschlüsselung mit dem RC4 Stromchiffrenalgorithmus\\
\lstinline|http://www.ws4d.org/ws4d-sec/2011/08/rc4-sha1-mac|: Signaturbildung: Hash mittels SHA1, Verschlüsselung des Hashes mittels RC4 Stromchiffrenalgorithmus\\
\lstinline|http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-128|: Verschlüsselung mit dem AES Blockchiffrenalgorithmus\\
\lstinline|http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-128-sha1-mac|: Signaturbildung: Hash mittels SHA1, Verschlüsselung des Hashes mittels AES Blockchiffrenalgorithmus
\par
\endgroup

Im \lstinline|<Body>| der Nachricht können Chiffren mittels \lstinline|<EncryptedData>|-Elementen eingebunden werden. Diese sind folgendermaßen aufgebaut:

\begin{lstlisting}[language=XML,caption={Ciphertext im Body der Nachricht}]
<ws4d:EncryptedData
    xmlns:ws4d="http://www.ws4d.org/ws4d-sec/2011/08"
    wsu:Id=...>
  <ws4d:CipherData>
    ...
  </ws4d:CipherData>
</ws4d:EncryptedData>
\end{lstlisting}
\lstinline|id| darf fehlen, wenn \lstinline|header/enc/@refs| leer ist oder fehlt.\\
\lstinline|<CipherData>| enthält die Chiffre in base64-codierter Form

\subsubsection{Beispielnachrichten}\label{cpt:appendix:profile:csec:samples}

Im folgenden ist eine beispielhafte SOAP-Nachricht in Listing~\ref{lst:app:sample:clear} dargestellt. Listing~\ref{lst:app:sample:cipher} zeigt diese Nachricht, die mit dem unter Abschnitt~\ref{cpt:appendix:profile:csec:signatures} beschriebenen Menchanismus signiert und dessen Body mit dem unter Abschnitt~\ref{cpt:appendix:profile:csec:encryption} beschrieben Menchanismus verschlüsselt wurde.

\begin{lstlisting}[language=XML,caption={Einfacher Request},label=lst:app:sample:clear]
<SOAP-ENV:Envelope
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Body>
    <ns:single-string-echo xmlns:ns="ws4d:ptest">
      <ns:in>01234</ns:in>
    </ns:single-string-echo>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{lstlisting}

Der \lstinline|<Body>| der Nachricht in Listing~\ref{lst:app:sample:clear} enthält genau ein Element mit dem Namen \lstinline|<single-string-echo>|. Sein Inhalt ist das eine Element \lstinline|<in>|, das die Zeichenkette \lstinline|01234| enthält. 

\begin{lstlisting}[language=XML,caption={Einfacher Request verschlüsselt und signiert},label=lst:app:sample:cipher,escapeinside={@}{@}]
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Header>
@\label{lst:app:sample:cipher:SecurityStart}@    <d:Security
        xmlns:d="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01">
@\label{lst:app:sample:cipher:EncStart}@      <d:Enc
          KeyId="ZHVtbXk="
          Refs=""
          Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes">
@\label{lst:app:sample:cipher:EncEnd}@      </d:Enc>
@\label{lst:app:sample:cipher:SigStart}@      <d:Sig
          KeyId="ZHVtbXlkdW1teWR1bW15ZHVtbXk="
          Refs=""
          Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-sha-mac"
          Sig="dtyuCvR713aKt3I4Fv/4bsUQcT7u6JkNvVL0yZFZwE0=">
@\label{lst:app:sample:cipher:SigEnd}@      </d:Sig>
@\label{lst:app:sample:cipher:SecurityEnd}@    </d:Security>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body wsu:id="bodyId">
@\label{lst:app:sample:cipher:EncryptedDataStart}@    <d:EncryptedData
        xmlns:d="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01"
        Id="bodyID">
@\label{lst:app:sample:cipher:CipherDataStart}@      <d:CipherData>
        ZS40Yxz0H72gN6l0OlNbtKNXw/E0ZlY0YpS2JpEK2q1fkgC... <!-- abgekuerzt -->
@\label{lst:app:sample:cipher:CipherDataEnd}@      </d:CipherData>
@\label{lst:app:sample:cipher:EncryptedDataEnd}@    </d:EncryptedData>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{lstlisting}

\noindent Zeilen~\ref{lst:app:sample:cipher:SecurityStart} bis~\ref{lst:app:sample:cipher:SecurityEnd}:  \lstinline|<Security>|-Element.\\
\noindent Zeilen~\ref{lst:app:sample:cipher:EncStart} bis~\ref{lst:app:sample:cipher:EncEnd}:  \lstinline|<Enc>|-Element, dass die Eigenschaften der verschlüsselten Passagen enthält. Das leere \lstinline|Refs|-Attribut impliziert, dass nur der Inhalt des \lstinline|<Body>|-Elements verschlüsselt wurde.\\
\noindent Zeilen~\ref{lst:app:sample:cipher:SigStart} bis~\ref{lst:app:sample:cipher:SigEnd}:  \lstinline|<Sig>|-Element, dass die Eigenschaften der Signatur und die Signatur selbst enthält. Das leere \lstinline|Refs|-Attribut impliziert, dass der Hash der gesamten SOAP-Nachricht als Basis der Signatur genutzt wird.\\
\noindent Zeilen~\ref{lst:app:sample:cipher:EncryptedDataStart} bis~\ref{lst:app:sample:cipher:EncryptedDataEnd}:  \lstinline|<EncryptedData>|-Element.\\
\noindent Zeilen~\ref{lst:app:sample:cipher:CipherDataStart} bis~\ref{lst:app:sample:cipher:CipherDataEnd}:  \lstinline|<CipherData>|-Element, das den verschlüsselten Inhalt des Bodys enthält.

\subsubsection{Vorgehensweise}\label{cpt:appendix:profile:csec:algorithm}

Im Folgenden werden die Schritte beschrieben, wie die beschriebene Lösung eingesetzt werden muss. Die Reihenfolge der einzelnen Schritte ist verbindlich. Ausgangspunkt ist die in Listing~\ref{lst:app:sample:clear} dargestellte Beispielnachricht.

\paragraph{Einfügen der Headerfelder}
In die Ausgangsnachricht wird der \lstinline|<Security>|-Header hinzugefügt. Er erhält ein leeres \lstinline|<Sig>|-Unterelement und -- falls Teile der Nachricht verschlüsselt werden sollen -- ein leeres \lstinline|<Enc>|-Unterelement. Es ensteht eine Datenstruktur wie unter Listing~\ref{lst:app:order:emptyHeaders} abgebildet (beachte Zeilen~\ref{lst:app:order:emptyHeaders:start} bis~\ref{lst:app:order:emptyHeaders:end}) 

\begin{lstlisting}[language=XML,caption={Ausgangsnachricht mit leeren Header-Feldern},label=lst:app:order:emptyHeaders,escapeinside={@}{@}]
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
@\label{lst:app:order:emptyHeaders:start}@  <SOAP-ENV:Header>
    <d:Security
        xmlns:d="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01">
      <d:Enc></d:Enc>
      <d:Sig></d:Sig>
    </d:Security>
@\label{lst:app:order:emptyHeaders:end}@  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <ns:single-string-echo xmlns:ns="ws4d:ptest">
      <ns:in>01234</ns:in>
    </ns:single-string-echo>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{lstlisting}

\paragraph{Attribute festlegen}
Nach Auswahl der zu verwendenden Algorithmen (Schemes) und Schlüssel (KeyIds), sowie der zu signierenden/verschlüsselnden Nachrichtenteile, werden die entsprechenden Attribute gesetzt. Das Attribut \lstinline|Security/Sig/@Sig| wird angelegt, bleibt jedoch leer. Es entsteht eine Datenstruktur wie in Listing~\ref{lst:app:order:filledHeaders} dargestellt (s. Zeilen~{\ref{lst:app:order:filledHeaders:enc:start}-\ref{lst:app:order:filledHeaders:enc:end}} und~{\ref{lst:app:order:filledHeaders:sig:start}-\ref{lst:app:order:filledHeaders:sig:end}}).

\begin{lstlisting}[language=XML,caption={Attribute der Headerfelder angelegt},label=lst:app:order:filledHeaders,escapeinside={@}{@}]
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Header>
    <d:Security
        xmlns:d="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01">
      <d:Enc
@\label{lst:app:order:filledHeaders:enc:start}@          KeyId="ZHVtbXk="
          Refs=""
@\label{lst:app:order:filledHeaders:enc:end}@          Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes">
      </d:Enc>
      <d:Sig
@\label{lst:app:order:filledHeaders:sig:start}@          KeyId="ZHVtbXlkdW1teWR1bW15ZHVtbXk="
          Refs=""
          Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-sha-mac"
@\label{lst:app:order:filledHeaders:sig:end}@          Sig="">
      </d:Sig>
    </d:Security>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <ns:single-string-echo xmlns:ns="ws4d:ptest">
      <ns:in>01234</ns:in>
    </ns:single-string-echo>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{lstlisting}

\paragraph{Berechnen der Signatur}
Die Nachricht in Listing~\ref{lst:app:order:filledHeaders} bildet die Grundlage zur Signaturberechnung. Falls das Attribut \lstinline|Security/Sig/@Refs| leer ist oder fehlt, wird der Digest über die gesamte Nachricht gebildet. Der Verschlüsselte Digest (also die Signatur) wird base64-codiert als Wert des Attributs \lstinline|Security/Sig/@Sig| festgelegt. Es entsteht die Nachricht wie in Listing~\ref{lst:app:order:filledSignature} (s. Zeile~\ref{lst:app:order:filledSignature:sig}).
\begin{lstlisting}[language=XML,caption={Signatur eingefügt},label=lst:app:order:filledSignature,escapeinside={@}{@}]
<SOAP-ENV:Envelope
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
      <SOAP-ENV:Header>
        <d:Security
            xmlns:d="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01">
          <d:Enc
              KeyId="ZHVtbXk="
              Refs=""
              Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes">
          </d:Enc>
          <d:Sig
              KeyId="ZHVtbXlkdW1teWR1bW15ZHVtbXk="
              Refs=""
              Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-sha-mac"
@\label{lst:app:order:filledSignature:sig}@              Sig="dtyuCvR713aKt3I4Fv/4bsUQcT7u6JkNvVL0yZFZwE0=">
          </d:Sig>
        </d:Security>
      </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <ns:single-string-echo xmlns:ns="ws4d:ptest">
      <ns:in>01234</ns:in>
    </ns:single-string-echo>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{lstlisting}

\paragraph{Verschlüsselung}
Falls eine Verschlüsselung durchgeführt wird, erfolgt diese im Anschluss an die Signaturbildung. Der Inhalt des \lstinline|<Body>|-Elements wird zur Chiffre $C$ verschlüsselt. Anschließend wird der Inhalt des \lstinline|<Body>|-Elements durch ein \lstinline|<EncryptedData>|-Element ersetzt. Sein Inhalt ist ein \lstinline|<CipherData>|-Element, das die Chiffre $C$ in base64-codierter Form enthält. Man erhält die Nachricht wie in Listing~\ref{lst:app:sample:cipher} dargestellt.

\subsection{WS Security Records}\label{cpt:appendix:profile:secrec}
Um die Performance zu steigern, indem Signaturbildung und Verschlüsselung durch eine gemeinsame kryptografische Routine bearbeitet werden, existieren die Security Records. Hierbei wird dem \lstinline|<Body>| ein Element \lstinline|<Record>| hinzugefügt. Format, Bearbeitung und Namensgebung orientieren sich an dem Record-Protokoll der Transport Layer Security (TLS) Protokollgruppe.

\subsubsection{Das \lstinline|<Record>|-Element}\label{cpt:appendix:profile:secrec:record}
Ein \lstinline|<Record>|-Element hat den folgenden Aufbau:
\begin{lstlisting}[language=XML,caption={Aufbau eines \lstinline|<Record>|-Elements},label=lst:app:records:record,escapeinside={@}{@}]
<Record
    CipherData="..." 
    EncRefs="..."
    KeyId="..."
    PrefixList="..."
    Scheme="..."
    SigRefs="...">
</Record>
\end{lstlisting}

\noindent Das \lstinline|<Record>|-Element hat keinen Inhalt. Die Informationen sind stattdessen in den Attributen enthalten:\\
\lstinline|CipherData| enthält die verschlüsselten Nachrichtenteile. Siehe dazu den folgenden Abschnitt~\ref{cpt:appendix:profile:secrec:cipherdata}.\\
\lstinline|EncRefs| hat die gleiche Bedeutung wie unter Abschnitt~\ref{cpt:appendix:profile:csec:encryption} beschrieben. Darf fehlen oder leer sein.\\
\lstinline|KeyId| enthält die ID des verwendeten Schlüssels\\
\lstinline|PrefixList| hat die gleiche Bedeutung wie unter WS-DD cpt. 8.2 beschrieben\\
\lstinline|Scheme| verwendetes Schema. Bisher sind folgende Schemes definiert: \\

\begingroup
\noindent
\leftskip=0.5cm % ggf. verstellen
\lstinline|http://www.ws4d.org/ws4d-sec/2011/08/rc4-sha1-mac|: Bildung des Digests mittels SHA-1, Verschlüsselung mittels RC4 Stromchiffre\\
\lstinline|http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-128-sha1-mac|: Bildung des Digests mittels SHA-1, Verschlüsselung mittels AES CBC 128bit\\
\par
\endgroup
\noindent \lstinline|SigRefs| hat die gleiche Bedeutung wie unter Abschnitt~\ref{cpt:appendix:profile:csec:signatures} beschrieben. Darf fehlen oder leer sein.

\subsubsection{Wert des \lstinline|<CipherData>|-Attributs}\label{cpt:appendix:profile:secrec:cipherdata}

Um den Wert des \lstinline|CipherData|-Attributs zu erhalten, wird die unter Listing~\ref{lst:app:records:cipherdatacontent} dargestellte Struktur befüllt, mittels des unter \lstinline|Record/Scheme| angegebenen Algorithmus verschlüsselt und base64-codiert.

\begin{lstlisting}[language=XML,caption={Inhalt des \lstinline|<CipherData>|-Attributs},label=lst:app:records:cipherdatacontent,escapeinside={@}{@}]
<Digest>...</Digest>
<Payload>...</Payload>
\end{lstlisting}

\noindent \lstinline|<Digest>| enthält den base64-codierten Hashwert (=Digest) der zu signierenden Nachrichtenteile.\\
\lstinline|<Payload>| Enthält die zu verschlüsselnden Elemente.

\subsubsection{Beispielnachricht}
Ausgangspunkt ist erneut der einfache Request, der in Listing~\ref{lst:app:sample:clear} abgebildet ist. Werden Vertraulichkeit, Integrität und Authentizität dieser Nachricht mittels eines Security Records mit dem Schema \lstinline|http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-sha-hmac| gewährleistet, entsteht die folgende Nachricht:

\begin{lstlisting}[language=XML,caption={Beispiel eines WS Security Records},label=lst:app:records:sample]
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Body>
    <Record
        CipherData="cLj9mvqgLVL/ty/7wqoaTvXfrlXggP8W[...]" <!-- abgekuerzt -->
        EncRefs=""
        KeyId="KeyId"
        PrefixList=""
        Scheme="http://www.ws4d.org/ws4d-sec/2011/08/aes-cbc-sha-hmac"
        SigRefs="">
    </Record>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
\end{lstlisting}

\subsubsection{Vorgehensweise}

Die Vorgehensweise zur Ersetzung des Klartextes durch einen Security Record wird im Folgenden beschrieben. Dieses Vorgehen beschreibt die Sendeseite. Auf Seite des Empfängers muss entsprechend in umgekehrter Reihenfolge vorgegangen werden.

\paragraph{Temporäre Datenstruktur}
Im Programmspeicher wird eine Datenstruktur $tmp$ entsprechend Listing~\ref{lst:app:records:cipherdatacontent} angelegt.

\paragraph{Digest der Nachricht}
Der Digest der zu signierenden Teile der Ausgangsnachricht wird berechnet und in die temporäre Datenstruktur $tmp$ eingetragen

\paragraph{Vertraulichkeit}
Die zu verschlüsselnden Nachrichtenteile werden in das Element \lstinline[mathescape]|$tmp$/Payload| verschoben. Sie sind darauf hin nicht mehr in der Ausgangsnachricht enthalten.

\paragraph{\lstinline|<Record>|-Element}
Dem \lstinline|<Body>| der Ausgangsnachricht wird ein \lstinline|<Record>|-Element hinzugefügt und die Attribute werden gesetzt. Das Attribut \lstinline|Record/@Cipherdata| enthält die base64-codierte Chiffre der temporären Datenstruktur $tmp$.

\section{Authentifizierung}

\subsection{Namespaces}
\lstinline|ws4d="http://www.ws4d.org"|

\subsection{Policies}

\subsubsection{Policy Name}

\lstinline|ws4d:AuthenticationMechanism|

\subsubsection{Struktur}

\begin{lstlisting}[language=XML,caption={Struktur der Policies},label=lst:app:records:sample]
(<wsp:Policy Name="AuthenticationMechanism" xmlns:wsp="http://www.w3.org/ns/ws-policy">
   <wsp:ExactlyOne>
     (<wsp:All>
        <!-- AuthenticationMechanism -->
      </wsp:All>)*
   </wsp:ExactlyOne>
 </wsp:Policy>)?
\end{lstlisting}

TBD: ggf. Definition und Unterstützung der Kompakten Policies

\subsubsection{Definierte Authentifizierungsmechanismen}
Subspezifikationen?!
\lstinline|ws4d:FlickerAuthentication|
\lstinline|ws4d:TappingAuthentication|
\lstinline|ws4d:PinAuthentication|
\lstinline|ws4d:BrokeredAuthentication|
More TBD, QR, NFC, ...
TODO: Harmonisierung mit z.B. \lstinline|http://www.ws4d.org/authentication/ui/mechanisms#flicker| (Anm.: Fällt aus. Geeinigt auf oberes, weil unteres nicht geklappt hat)

Spezialfall: \lstinline|ws4d:EncryptedPinExchange| Pin wird numerisch im Klartext in die Nachrichten geschrieben. Darf (vermutlich) ausschließlich von (UI-)Authentifiziererklasse genutzt werden. Darf nur angefragt werden, wenn request verschlüsselt erfolgt. Darf ausschließlich verschlüsselt beantwortet werden. Nur zwischen (UI-)Authentifizierern verwendet werden, die eine Vertrauensbeziehung miteinander pflegen, um Nutzer zu entlasten. Angesprochener Dienst MUSS mit Fehler antworten, wenn unverschlüsselt angefragt.


\subsection{Porttypes}

\subsubsection{Geräte}

\paragraph{Authentifizierer}
Übernimmt Brokered Authentication, Typ: \lstinline|ws4d:Authenticator|, hat einen Dienst vom Typ \lstinline|ws4d:TODO...Nocht nicht klar|

\paragraph{UI-Authentifizierer}
Übernimmt Indirect Authentication, Typ: \lstinline|ws4d:UI-Authenticator|, hat einen Dienst vom Typ \lstinline|ws4d:AuthenticatedEllipticCurveDiffieHellman|

\paragraph{Endpunkt}
Zur direkten Authentifizierung. Typ: \lstinline|ws4d:AuthenticationEndpoint|, hat einen Dienst vom Typ \lstinline|ws4d:AuthenticatedEllipticCurveDiffieHellman|

\subsubsection{Dienste}

TODO: WST schreibt für z.B. Issue-Binding irgendwelches Addressing-Zeugs vor. Überprüfen, ob umgesetzt.

\paragraph{ECDH}
Dienst vom Typ \sout{\lstinline|ws4d:AuthenticatedEllipticCurveDiffieHellman|} \lstinline|wst:Issue|, bietet \sout{die Methoden ECC\_DH1 und ECC\_DH2 (Parameter kommen noch)} Methoden mit den \lstinline|wsa:Action|s, wie sie für \lstinline|wst:Issue| definiert sind und über die ein authentifizierter ECDH-Schlüsselaustausch stattfinden kann. (Der Name ECC\_DH gibt den Hinweis auf ECC).
Nicht vergessen, OOB-PIN in ECDH1-Rückgabe zu übernehmen. Nur wenn verschlüsselt(!!). Ist wichtig zur Verknüpfung von UI-Geräten. Sonst müsste die PIN OOB von einem Handy zum anderen, obwohl die sicher miteinander kommunizieren können. Das wäre ziemlicher Quatsch.

\subparagraph{Die Authentifizierungsparameter}

d,fhg

\begin{lstlisting}[language=XML,caption={AuthECCDHParameters},label=lst:auth:ECCParam]
<ws4d:AuthECCDHParameters>
  <ws4d:auth-ecc-dh-curvename>
  <ws4d:auth-ecc-dh-nonce></ws4d:auth-ecc-dh-nonce>
  <ws4d:auth-ecc-dh-public-key></ws4d:auth-ecc-dh-public-key>
  <ws4d:auth-ecc-dh-cmac></ws4d:auth-ecc-dh-cmac>
  <ws4d:auth-ecc-dh-numerical-pin></ws4d:auth-ecc-dh-numerical-pin>
</ws4d:AuthECCDHParameters>
\end{lstlisting}


\subparagraph{Das RequestedSecurityToken und SecurityContextToken}


\begin{lstlisting}[language=XML,caption={RequestedSecurityToken},label=lst:auth:RST]
<wst:RequestedSecurityToken>
  <wsc:SecurityContextToken>
    <wst:Identifier> <!-- gibt es das..? Anm: gibt auf jeden Fall wsc:Identifier -->
    <wst:SignWith>
    <wst:EncryptWith>
  </wsc:SecurityContextToken>
</wst:RequestedSecurityToken>
\end{lstlisting}


\subparagraph{ECC\_DH1} Methode für erste Hälfte des Handshakes. Das RequestedSecurityToken wird schon mitgegeben, damit Parameter angefragt werden können (Algorithmen, Kurvennamen, ...)

ws4d:Brokerable: Gibt es irgendwie nicht schöner. Delegatable und Forwardable beziehen sich immer auf Tokens.

In:
\begin{lstlisting}[language=XML,caption={Struktur eines ECC\_DH1-Requests},label=lst:auth:ecc_dh1]
<wst:RequestSecurityToken>
  <wst:TokenType></wst:TokenType>
  <wst:RequestType></wst:RequestType>
  <wsp:AppliesTo></wsp:AppliesTo>
  <wst:OnBehalfOf></wst:OnBehalfOf>
  <ws4d:AuthenticationType></ws4d:AuthenticationType>
  <wst:SignWith>
  <wst:EncryptWith>
  <ws4d:Brokerable></ws4d:Brokerable>
  <ws4d:AuthECCDHParameters>
    [...]
  </ws4d:AuthECCDHParameters>
</wst:RequestSecurityToken>
\end{lstlisting}

Out:
\begin{lstlisting}[language=XML,caption={Struktur eines ECC\_DH1-Response},label=lst:auth:ecc_dh1_response]
<wst:RequestSecurityTokenResponse>
  <wst:TokenType></wst:TokenType>
  <wst:RequestType></wst:RequestType>
  <wsp:AppliesTo></wsp:AppliesTo>
  <wst:OnBehalfOf></wst:OnBehalfOf>
  <ws4d:AuthenticationType></ws4d:AuthenticationType>
  <ws4d:AuthECCDHParameters></ws4d:AuthECCDHParameters>
  <wst:RequestedSecurityToken>
    [...]
  </wst:RequestedSecurityToken>
</wst:RequestSecurityTokenResponse>
\end{lstlisting}


\subparagraph{ECC\_DH2} Zweite Hälfte

In:
\begin{lstlisting}[language=XML,caption={Struktur eines ECC\_DH2-Request},label=lst:auth:ecc_dh2_request]
<wst:RequestSecurityTokenResponse>
  [...] <!-- s.o. -->
</wst:RequestSecurityTokenResponse>
\end{lstlisting}

Out:
\begin{lstlisting}[language=XML,caption={Struktur eines ECC\_DH2-Response},label=lst:auth:ecc_dh2_response]
<wst:RequestSecurityTokenResponseCollection>
  <wst:RequestSecurityTokenResponse>
    [...] <!-- s.o. -->
  </wst:RequestSecurityTokenResponse>
</wst:RequestSecurityTokenResponseCollection>
\end{lstlisting}

\paragraph{Brokered}

Der selbe Dienst, das selbe Binding, aber mit einer Methode, der \lstinline|wsa:Action|, wie sie in wsc, cpt.3 definiert ist.

\lstinline|RequestedSecurityTokenResponse| enthält ein \lstinline|RequestedSecurityToken| genau wie für ECDH, jedoch zusätzlich ein \lstinline|wst:RequestedProofToken|, in dem der Schlüssel als \lstinline|wst:BinarySecret| abgelegt ist, (wie in 4.4.6 beschrieben - tolle Stelle um Spec zu kürzen.).

Komplette Antwort:
\begin{lstlisting}[language=XML,caption={Antwort auf RST},label=lst:auth:ecc_dh2_request]
<wst:RequestSecurityTokenResponseCollection>
  <wst:RequestSecurityTokenResponse>
    <wst:RequestedSecurityToken>
      <wsc:SecurityContextToken>
      </wsc:SecurityContextToken>
    </wst:RequestedSecurityToken>
    <wst:RequestedProofToken>
      <wst:BinarySecret type=http://docs.oasis-open.org/ws-sx/ws-trust/200512/SymmetricKey>
      <!-- type optional, ist eh Default -->
      <!-- s. wst,cpt3.3 -->
    </wst:RequestedProofToken>
  </wst:RequestSecurityTokenResponse>
</wst:RequestSecurityTokenResponseCollection>
\end{lstlisting}

\section{Autorisierung}
\begin{itemize}
\item Deckt sich in weiten Teilen mit \cite[cpt.9]{WS-Federation}.

\item Was hier die Bindings angeht: Das muss wohl alles (Authentifizierung und Autorisierung) über den gleichen Dienst und sogar mit den gleichen Bindings (Issue) laufen. Die Unterscheidung wird dann über den TokenType geregelt. Über die Gerätetypen lässt sich regeln, wer welche Tokentypes unterstützt (wer also Autorisierer, Authentifizierer oder beides ist).

\item In \cite[9.3]{WS-Federation} steht, dass wohl zahllose Claim-Dialekte existieren und das daher der Common Claim Dialect wie in \cite[9.3]{WS-Federation} als Interoperabilitätsbasis definiert wird.

\item Das Conditions-Element von ner SAML Assertion (\cite[cpt.2.5]{SAML2Core}) wirkt auf den zweiten Blick gar nicht so uninteressant. Eine Autorisierung/Assertion darf sich auf jede erdenkliche Weise einschränken lassen. So kann eine Autorisierung innerhalb einer Zeitspanne und/oder nur für einmalige Nutzung ausgestellt werden. Eine Assertion kann auch Einschränkungen bzgl. seines Audience haben (also an wen sie gerichtet ist) und bzgl. Proxies (wen sie berechtigt, weiter zu delegieren.)

\item Desweiteren Extensibility Mechanismus - man kann also so ziemlich alles an Kontext-Informationen da rein packen.

\item SAML ()\cite[cpt.8.1.2]{SAML2Core}) definiert auch die Actions, die erlaubt sind: Read / Write / Execute / Delete / Control jeweils mit Negator (\textasciitilde). Dort gibt es auch noch andere Action- / Autorisierungsmuster. Welches genutzt wird, wird durch den Namespace angegeben, der bei Action angegeben wird, dadurch sind auch andere Muster möglich.

\item Eine SAML-Assertion enthält eine Signatur. Ohne wird es auch nicht gehen. Wird dann durch eine CompactSignature ersetzt.

\item Bootstrap: Wird erst mal so gelöst, dass sich Broker auch als Autorisierer anbieten. Clients nehmen des ersten authentifizierten Broker als primären Autorisierer.

\end{itemize}